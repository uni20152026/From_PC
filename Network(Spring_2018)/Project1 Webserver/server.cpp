#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>

#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include "threadpool/ThreadPool.h"

#define PORT 5000

void *respond (int sock);
char *ROOT;

int main( int argc, char *argv[] ) {
  int sockfd, newsockfd, portno = PORT;
  socklen_t clilen;
  struct sockaddr_in serv_addr, cli_addr;
  clilen = sizeof(cli_addr);
  ROOT = getenv("PWD");

  /* First call to socket() function */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if (sockfd < 0) {
    perror("ERROR opening socket");
    exit(1);
  }

  // port reusable
  int tr = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &tr, sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }

  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

  /* TODO : Now bind the host address using bind() call.*/

  //**************************************************************
  if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
   {
      perror("ERROR on binding");
      exit(1);

   }
  //************************************************************** 

  /* TODO : listen on socket you created */
   
  //*************************************************************
  listen(sockfd,50);
  //**************************************************************


  printf("Server is running on port %d\n", portno);

  ThreadPool thread(5);

  while (1) {
    /* TODO : accept connection */
    // newsockfd =
    //************************************************************
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

    if (newsockfd < 0) 
    {
      perror("ERROR on accept");
      exit(1);
    }

    thread.enqueue([newsockfd] {respond(newsockfd);});
  }

  return 0;
}

void *respond(int sock) {
  int n;
  char buffer[9999];
  char abs_path[256];

  bzero(buffer,9999);
  n = recv(sock,buffer,9999, 0);
  //std::cout << buffer << " \n";
  if (n < 0) {
    printf("recv() error\n");
    return NULL;
  } else if (n == 0) {
    printf("Client disconnected unexpectedly\n");
    return NULL;
  } else {
    char* method = strtok(buffer, " \n\t");
    std::string path;
    int num = 0;

    while(method != NULL)
    {
      if(num == 1)
        path = method;

      num ++;
      method = strtok(NULL, " \n\t");
    }
   
    //***************************************************
    
    std::string file_path (ROOT);

    if(path == "/")
    {
      path += "index.html";
    }
    path = ROOT + path;
    std::string header = "";
    header += "HTTP/1.1 200 OK\r\n";
    header += "Content-Type: text/html\r\n";
    //header += "Content-Type: image/jpeg\r\n";
    header += "Content-Encoding = UTF-8\r\n";
    //header += "Content-Encoding = binary\r\n";
    header += "Connection: close \r\n";
    header += "\r\n";

    int s = write(sock, header.c_str(), strlen(header.c_str()));
    FILE *file; 
    long lSize;
    char * buf;
    size_t result;

    //file.open(file_path.c_str(), std::ios::binary);
    file = fopen(path.c_str(), "rb");
    if(file == NULL)
    {
      std::cout << " ***** WARNING!!! Cannot open file " << path.c_str() <<"\n";
      return NULL;
    }
    
    fseek(file, 0, SEEK_END);
    lSize = ftell(file);
    rewind(file);

    buf = (char*) malloc (sizeof(char)*lSize);
    if(buf == NULL)
      std::cout << "ERROR !!! buf is empty \n";

    result = fread(buf,1, lSize, file);
    write(sock, buf, result);
    
    fclose(file);
    delete buf;
  }

  shutdown(sock, SHUT_RDWR);
  close(sock);

}