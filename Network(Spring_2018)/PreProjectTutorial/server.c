#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>

#include <string.h>
void respond(void* newsockfd);

int main( int argc, char *argv[] ) {
   int sockfd, newsockfd, portno, clilen;
   char buffer[256];
   struct sockaddr_in serv_addr, cli_addr;
   int  n;
   
   /* Initialize socket structure */
   bzero((char *) &serv_addr, sizeof(serv_addr));
   portno = 5001;
   
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno);

   clilen = sizeof(cli_addr);
   
   // TODO : create socket and get file descriptor
   sockfd = socket(AF_INET, SOCK_STREAM, 0);



   void respond(void* newsockfd);
   pthread_t thread[9999];
   

   // TODO : Now bind the host address using bind() call.
   if(bind(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
   {
      perror("ERROR on binding");
      exit(1);
   }
   // TODO : start listening for the clients,
   // here process will go in sleep mode and will wait for the incoming connection
   listen(sockfd, 5);
   // TODO: Accept actual connection from the client

   // TODO : inside this while loop, implement communicating with read/write or send/recv function 
   int count = 0;
   do {
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
    if(newsockfd < 0)
    {
      printf("ERROR, could not accept from client \n");
      //cout << "ERROR, could not accept from client \n"; 
      break;
    }


    int create_thread = pthread_create(&thread[count ++], NULL, respond, (void *) newsockfd); 
    if(create_thread == 0)
    {
      printf("ERROR, could not create a thread \n");
      //cout << "ERROR, could not create thread \n";
    }
     // ==> here
     
     // TODO escape this loop, if the client sends message "quit"
   } while (1);
      
   return 0;
}

void respond(void* newsockfd)
{
    char buff[256];
    bzero(buff, 256);
     int nn;
     nn = read(newsockfd, buff, 256);

     if(nn < 0)
     {
      perror("ERROR reading from socket");
      exit(1);
     }

     printf("client said : %s\n",buff );

     scanf("%s", buff);
     nn = write(newsockfd, buff, strlen(buff));
     if(nn < 0)
     {
      perror("ERROR writing to socket");
      exit(1);
     }

     if(!bcmp(buff, "quit", 4))
        return NULL;
}