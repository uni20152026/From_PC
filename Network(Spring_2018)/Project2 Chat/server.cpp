#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <vector>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>

#include <string>
#include <iostream>
#include <fstream>
#include <iostream>
#include <map>
#include <utility>
#include <cstdlib>
#include <iterator>
//#include <windows.h>

using namespace std;
//#include <thread>
//#include "threadpool/ThreadPool.h"
#include <pthread.h>  

#define PORT 5000

typedef map <int, pair<string, int> > myMap;
myMap data;


vector<string> split_function(vector<string> tokens, string str ,char split_char);
void send_mess_all(int sock_s, string msg);
void *respond (void* sock);
void timer(int sec);

char *ROOT;

int main( int argc, char *argv[] ) {
  int sockfd, newsockfd, portno = PORT;
  socklen_t clilen;
  struct sockaddr_in serv_addr, cli_addr;
  clilen = sizeof(cli_addr);

  /* First call to socket() function */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if (sockfd < 0) {
    perror("ERROR opening socket");
    exit(1);
  }

  // port reusable
  int tr = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &tr, sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }

  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

  /* TODO : Now bind the host address using bind() call.*/

  //**************************************************************
  if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
   {
      perror("ERROR on binding");
      exit(1);

   }
  //************************************************************** 

  /* TODO : listen on socket you created */
   
  //*************************************************************
  listen(sockfd,50);
  //**************************************************************


  //printf("Server is running on port %d\n", portno);

  pthread_t thread[9999];

  /*while (1) {
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

    if (newsockfd < 0) 
    {
      perror("ERROR on accept");
      exit(1);
    }

    thread.enqueue([newsockfd] {respond(newsockfd);});
  }*/

  int count = 0;
  do {
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
    if(newsockfd < 0)
    {
      //printf("ERROR, could not accept from client \n");
      cout << "ERROR, could not accept from client \n"; 
      break;
    }


    int create_thread = pthread_create(&thread[count ++], NULL, respond, (void *) newsockfd); 
    //int create_res = pthread_create(&threads[num ++], NULL, listenToClient, (void *) client);
    if(create_thread)
    {
      //printf("ERROR, could not create a thread \n");
      cout << "ERROR, could not create thread \n";
    }
     // ==> here
     
     // TODO escape this loop, if the client sends message "quit"
   } while (1);

  return 0;
}

void *respond(void* sock) {
    char buff[256];
     int nn;
     string message;
     vector<string> token;
     

     while(1)
     {

      bzero(buff, 256);
      nn = read((long) sock, buff, 256);
      if(nn < 0)
      {
        perror("ERROR reading from socket");
        exit(1);
      }
      //printf("client said : %s\n",buff );
     
     message = (string) buff;
     //vector<string> token;
     token = split_function(token, message, ' ');
     //cout << "----------------------" << endl;

      /*
      for (int i=0; i< token.size(); i++)
      {
          cout << i+1 << "th string is: "<< token[i] << " , ";
      }
      cout << endl;
      */
     
     if(token[0] == "/connect")
     {
        if(token.size() == 3)
        {
          int roomNumber = stoi(token[1]);
          string nickName = token[2];
          //cout << "I am in /connect, my name is: " << nickName << endl;

          for(myMap::const_iterator iter = data.begin(); iter != data.end(); ++iter)
          {
            //cout << "Names in data are: " << iter->second.first << endl;
            if(iter->second.first == nickName)
            {
              nickName = nickName + "_2";  
            }
          }
          
          pair <string, int> name_room;
          name_room.first = nickName;
          name_room.second = roomNumber;

          //************** Notification about connection *******
          
          string mess = nickName + " joined room #" + token[1];
          for(myMap::const_iterator iter1 = data.begin(); iter1 != data.end(); ++iter1)
          {
            int vvv = write(iter1->first, mess.c_str(), mess.length());
            if(vvv < 0)
            {
              perror("ERROR writing to socket, could not send message to all clients");
              exit(1);
            }
          }


          //****************************************************

          data[(long) sock] = name_room; 

          string response_m = "Hello " + nickName + "! This is room #" + token[1];
          
          int mm = write((long) sock, response_m.c_str(), response_m.length());
          if(mm < 0)
          {
            perror("ERROR writing to socket after connecting to chat");
            exit(1);
          }

          //  HAVE TO SEND NOTIFICATION TO ALL USERS
          //  HAVE TO SEND NOTIFICATION TO ALL USERS
        }
        else
        {
          cout << "ERROR, you are trying to connect to chat in a wrong way" << endl;
        }
     }
     else if(token[0] == "/quit")
     {
        /*
        pair <string, int> pp;
        pp = data.find((long) sock);

        if(pp == map::end)
        {
          cout << "ERROR, you are nor in the chat"
        }
        */

        string nickName = data.find((long) sock)->second.first;
        int numm = data.find((long) sock)->second.second;
        string numm_str = std::to_string(numm);

        //cout << "***** You are in the quit function, name of the client is: " << nickName << endl;
        string response_m = "Goodbye " + nickName;
          
        int mm = write((long) sock, response_m.c_str(), response_m.length());
        if(mm < 0)
        {
          perror("ERROR writing to socket after connecting to chat");
          exit(1);
        }

        //cout << "++++++" << endl;
        data.erase((long) sock);
        

        //************** Notification about connection *******
          
        string mess = nickName + " disconnected from room #" + numm_str;
        for(myMap::const_iterator iter1 = data.begin(); iter1 != data.end(); ++iter1)
        {
          int vvv = write(iter1->first, mess.c_str(), mess.length());
          if(vvv < 0)
          {
            perror("ERROR writing to socket, could not send message to all clients");
            exit(1);
          }
        }


        //****************************************************


        break;

        //cout << "------" << endl;

        //  HAVE TO SEND NOTIFICATION TO ALL USERS
        //  HAVE TO SEND NOTIFICATION TO ALL USERS
     }
     else if(token[0] == "/list")
     {
      int room_num = data.find((long) sock)->second.second;
      string room_num_str = std::to_string(room_num);
      string response_mm = "This is list of users in room #" + room_num_str + " \r\n";
      
      int mm = write((long) sock, response_mm.c_str(), response_mm.length());
      if(mm < 0)
      {
        perror("ERROR writing to socket (/list)");
        exit(1);
      }

      int counter = 1;
      for(myMap::const_iterator iter = data.begin(); iter != data.end(); ++iter)
      {
        if(iter->second.second == room_num)
        {
          string num = std::to_string(counter);
          string response_mes = num + ". " + iter->second.first + "\r\n";

          int zz = write((long) sock, response_mes.c_str(), response_mes.length());
          if(zz < 0)
          {
            perror("ERROR writing to socket (/list)");
            exit(1);
          }

          counter ++;
        }     
      }

     }
     else if(token[0] == "/join")
     {
        int roomNum = stoi(token[1]);

        //cout << "Room number before changing: "<<data.find((long) sock)->second.second << endl;
        data.find((long) sock)->second.second = roomNum;
        //cout << "Room number after changing: "<<data.find((long) sock)->second.second << endl;
     }
     else if (token[0] == "All")
     {
        string mess_to_all = "";

        for (int i=2; i< token.size(); i++)
        {
          mess_to_all = mess_to_all + token[i] + " ";
        }

        send_mess_all((long) sock, mess_to_all);
        

      }
      else
      {
        /*
        for (int i=0; i< token.size(); i++)
          cout << i+1 << "th is: " << token[i] << " , ";
        cout << endl;
        */

        int woha = 0;

        vector<string> receiver;
        string msg_to_rcv = "";

        while(token[woha] != ":")
        {
          receiver.push_back(token[woha]);
          woha ++;
        }

        woha ++;
        for (woha; woha < token.size(); woha++)
        {
          msg_to_rcv = msg_to_rcv + token[woha] + " ";
        }

        for(int i = 0; i < receiver.size(); i++)
        {
          size_t found = receiver[i].find(",");
          if (found!=std::string::npos)
          {
            receiver[i].resize(receiver[i].length() - 1);
          }
            
            //cout << receiver[i] << " ";
          
        }
        
        //cout << endl;

        
        size_t found_1 = receiver[0].find("#");
        if (found_1!=std::string::npos)
        {
          vector<string> parse;

          for(int i = 0; i < receiver.size(); i++)
          {
            parse = split_function(parse, receiver[i], '#');
            string name_recv = parse[0];
            int time_to_wait = stoi(parse[1]);

            //cout << "Server have to sleep " << time_to_wait << " seconds" << endl;

            for(myMap::const_iterator iter = data.begin(); iter != data.end(); ++iter)
            {
              if(iter->second.first == name_recv)
              {
                //recv[i] = true;
                string temp = data.find((long) sock)->second.first + ": ";
                msg_to_rcv = temp + msg_to_rcv;
                sleep(time_to_wait);
                //timer(time_to_wait);
                int zz = write(iter->first, msg_to_rcv.c_str(), msg_to_rcv.length());
                if(zz < 0)
                {
                  perror("ERROR writing to socket (/list)");
                  exit(1);
                }  
              }
            }

            parse.clear();
          }  
        }
        else
        {
        
          bool recv[receiver.size()];
          for (int j = 0; j < receiver.size(); ++j)
          {
            recv[j] = false;
          }
          
          //int rrrr = 1;
          for(int i = 0; i < receiver.size(); i++)
          {
            for(myMap::const_iterator iter = data.begin(); iter != data.end(); ++iter)
            {
              if(iter->second.first == receiver[i])
              {
                recv[i] = true;
                string temp = data.find((long) sock)->second.first + ": ";
                msg_to_rcv = temp + msg_to_rcv;
                int zz = write(iter->first, msg_to_rcv.c_str(), msg_to_rcv.length());
                if(zz < 0)
                {
                  perror("ERROR writing to socket (/list)");
                  exit(1);
                }  
              }
            }
          }

          for (int j = 0; j < receiver.size(); ++j)
          {
            if(recv[j] == false)
            {
              string messg = "There is no such user: " + receiver[j];
              int zzz = write((long) sock, messg.c_str(), messg.length());
              if(zzz < 0)
              {
                perror("ERROR writing to socket (/list)");
                exit(1);
              }  
            }
          }

        }
        
        //
      }
      

     


     /*
     bzero(buff, 256);
     scanf("%s", buff);
     nn = write((long) sock, buff, strlen(buff));
     if(nn < 0)
     {
      perror("ERROR writing to socket");
      exit(1);
     }
     */

     token.clear();
   }
}

vector<string> split_function(vector<string> tokens, string str ,char split_char)
{
    istringstream split(str);
    for (string each; getline(split, each, split_char); tokens.push_back(each));
    return tokens;
}

void send_mess_all(int sock_s, string msg)
{
  string NickN = data.find(sock_s)->second.first;
  msg = NickN + ": " + msg;

  for(myMap::const_iterator iter1 = data.begin(); iter1 != data.end(); ++iter1)
  {
    int vv = write(iter1->first, msg.c_str(), msg.length());
    if(vv < 0)
    {
      perror("ERROR writing to socket, could not send message to all clients");
      exit(1);
    }
  }
}

void timer(int sec)
{
    sleep(sec*1000);
}