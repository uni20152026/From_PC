#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

void *func_for_writing(void *qwerty1);
void *func_for_reading(void *qwerty2);

bool is_quit = false;	
int main(int argc, char *argv[]) {
   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
   struct hostent *server;
   
   char buffer[256];
   portno = 5000;
   
   server = gethostbyname("127.0.0.1");
   
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
   }

   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
   
   // TODO : create socket and get file descriptor 

   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   server = gethostbyname("127.0.0.1");

   if(server == NULL)
   {
   	fprintf(stderr, "ERROR, no such host\n");
   	exit(0);
   }
   
   // TODO : connect to server with server address which is set above (serv_addr)
   if(connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) 
   {
   	perror("ERROR connecting");
   	exit(1);
   };

   // TODO : inside this while loop, implement communicating with read/write or send/recv function 
   
     //printf("At first you have to connect to chat, ex: ");
    if(argc > 4)
   	{
   		cout << "ERROR, too much arguments" << endl;
   		return 0;
   	}
   	else if(argc < 4)
   	{
   		cout << "ERROR, not enough arguments" << endl;
   		return 0;
   	}

   	string nickName = argv[3];
   	string roomNumber = argv[2];

    string first_message = "/connect " + roomNumber + " " + nickName;
     //getline(cin, first_message);

     //bzero(buffer,256);
     //scanf("%s", buffer);

     n = write(sockfd, first_message.c_str(), first_message.length());
     if(n < 0)
     {
      perror("ERROR writing to socket");
     }

     /*bzero(buffer,256);
     n = read(sockfd, buffer, 255);
     if(n < 0)
     {
      perror("ERROR reading from socket");
      exit(1);
     }
     
     printf("server replied reading from socket: %s \n", buffer);
	*/
     
     pthread_t th_for_writing;
     pthread_t th_for_reading;

     int nn;
     nn = pthread_create(&th_for_writing, NULL, func_for_writing, (void *) sockfd);
     if(nn != 0)
     {
     	cout << "ERROR, could not crate a thread for writing from client to server" << endl;
     	exit(0);
     }
     
     /*
     else
     	cout << "THREAD for writing from client to server has been successfully created" << endl;
	*/

     nn = pthread_create(&th_for_reading, NULL, func_for_reading, (void *) sockfd);
     if(nn != 0)
     {
     	cout << "ERROR, could not create a thread for reading from server " << endl;
     	exit (0);
     }
     
     /*
     else
     	cout << "THREAD for reading from server has been successfully created" << endl;
	*/

     
     void *pp;
     nn = pthread_join(th_for_writing, &pp);
     if(nn != 0)
     {
     	cout << "ERROR, could not join a thread for writing with main thread" << endl;
     
     }

     nn = pthread_join(th_for_reading, &pp);
     if(nn != 0)
     {
     	cout << "ERROR, could not join a thread for reading with main thread" << endl;
     }
	

     close (sockfd);

     // ==> here
     
     // TODO escape this loop, if the server sends message "quit"
   
   
   return 0;
}

void *func_for_writing(void *sock)
{
	//cout << "*** Thread for writing to server is running" << endl;
	
	char buff[256];
	while(1)
	{
		cin.getline(buff, 256);
		if(strlen(buff) > 0)
		{
			int mm = write((long) sock, buff, strlen(buff));
			if(mm < 0)
     		{
      			perror("ERROR writing to socket");
     		}

     		string command = (string) buff;
     		if(command == "/quit")
     		{
     			is_quit = true;
     			pthread_exit(NULL);
     		}
		}

	}

	return NULL;
}

void *func_for_reading(void *sock)
{
	//cout << "*** Thread for reading from server is running" << endl;
	
	char buff[256];
	while(1)
	{
		if(is_quit)
		{
			pthread_exit(NULL);
		}
		
		bzero(buff,256);
     	int mm = read((long) sock, buff, 255);
     	if(mm < 0)
     	{
      		perror("ERROR reading from socket");
      		exit(1);
     	}

     	printf("%s \n", buff);

	}

	//delete[] buff;
	return NULL;
}