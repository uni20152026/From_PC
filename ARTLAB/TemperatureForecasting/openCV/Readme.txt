image_filtering.py is a OpenCV program which filters temperature map.
This program takes as input: image title, resolution(ex: 5*5) and asks 
wheather to fill "bad" cells with approximate color or black color.
In the end it returns filtered image and prints out temperature value of each cell.
	Image_filtering.py does:
1) Cuts bottom part of an image.

2) Divides image into cells according to input. 
If a cells is "Good" then it is filled with average color of that cell. 
If a cell is "Bad" then it is filled with the approximate color 
or the black color according to the choice of the user. 
Then program gets RGB value of each cell and matches it to the closest temperature. 
Thus, program finds temperature value of each cell. It is done by using a table shawn below.

3) In the end program returns filtered image(also saves it) 
and prints out temperature value and color(RGB value) of each cells.

Note: "Bad" cell is the cell in which NOT temperature pixels are more than temperature pixels. 
"Good" cell is the cell which is not "Bad".

************* (Temperature - RGB) TABLE *************
Temperature - RGB 
-40 - 204,204,204
-39 - 204,204,204
-38 - 204,204,204
-37 - 204,204,204
-36 - 204,204,204
-35 - 208,196,208
-34 - 213,187,214
-33 - 215,181,214
-32 - 213,175,216
-31 - 215,169,215
-30 -213,163,214
-29 - 212,139,212
-28 - 214,112,214
-27 -215,87,210
-26 - 215,62,214
-25 - 200,62,201
-24 - 187,63,187
-23 - 183,60,176
-22 - 168,63,166
-21 - 156,67,159
-20 - 148,66,148
-19 - 140,67,140
-18 - 135,65,135
-17 - 102,69,126
-16 - 77,68,121
-15 - 84,76,125
-14 - 88,80,127
-13 - 100,94,138
-12 - 111,104,146
-11 - 111,107,157
-10 - 113,109,168
-9 - 111,112,166
-8 - 109,114,170
-7 - 106,119,174
-6 - 101,124,178
-5 - 100,134,180
-4 - 95,143,181
-3 - 98,150,174
-2 - 100,156,169
-1 - 103,163,161
0 - 107,170,153
1 - 108,174,136
2 - 108,177,120
3 - 105,181,109
4 - 105,184,101
5 - 115,187,105
6 - 125,188,109
7 - 137,189,107
8 - 152,192,103
9 - 162,196,101
10 - 176,198,98
11 - 183,200,96
12 - 193,202,97
13 - 197,198,96
14 - 201,196,94
15 - 203,190,94
16 - 202,185,95
17 - 202,180,94
18 - 201,172,94
19 - 204,166,95
20 - 201,159,99
21 - 203,153,102
22 - 201,147,103
23 - 201,141,104
24 - 200,135,105
25 - 198,126,111
26 - 196,118,116
27 - 193,109,124
28 - 191,101,129
29 - 182,95,127
30 - 175,90,123
31 - 171,91,120
32 - 165,89,117
33 - 163,84,115
34 - 159,77,113
35 - 154,72,108
36 - 149,69,106
37 - 138,69,97
38 - 127,68,90
39 - 125,68,85
40 - 123,67,80
41 - 120,67,73
42 - 117,67,68
43 - 117,66,73
44 - 113,66,74
45 - 109,66,76
46 - 108,64,81
47 - 102,62,73
48 - 95,60,67
49 - 88,56,61
50 - 